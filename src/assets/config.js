export var siteConfig = {
  serviceUrl: 'submission/create',
  pages: [
    {
      pageId: 'home',
      serviceUrl: "https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate",
      title: 'Home',
      fields: [
        {
          name: 'logo',
          type: 'img',
          src: 'assets/1536798901299.png'
        },
        {
          name: 'header',
          fields: [
            {
              name: 'submit',
              title: 'Submit',
              type: 'link',
              href: 'https://dt.rebatepromotions.com/#/home'
            },
            {
              name: 'track',
              title: 'Track',
              type: 'link',
              href: '/tracker'
            },
            {
              name: 'faq',
              title: 'FAQ',
              type: 'link',
              href: '/faq'
            },
            {
              name: 'contact',
              title: 'Contact Us',
              type: 'link',
              href: '/contact-us'
            }
          ]
        },
        {
          name: 'banner',
          type: 'img',
          src: 'assets/banner.png'
        },
        {
          name: 'footer',
          title: '© 2020 Reinalt Thomas',
          fields: [
            {
              name: 'legal',
              title: 'Legal',
              type: 'link',
              href: '#'
            },
            {
              name: 'privacy',
              title: 'Privacy Policy',
              type: 'link',
              href: '#'
            },
            {
              name: 'faq',
              title: 'FAQ',
              type: 'link',
              href: 'https://dt.rebatepromotions.com/#/faqs'
            },
            {
              name: 'contact',
              title: 'Contact Us',
              type: 'link',
              href: 'https://dt.rebatepromotions.com/#/contact'
            }
          ]
        },
        {
          name: 'submission',
          fields: [
            {
              name: 'txtSubmit',
              title: "Welcome to the Discount Tire Rebate Center. Enter your offer code and purchase date to begin your submission.<br /><br />Click <a href='https://www.discounttire.com/promotions' target='_blank'>here</a> to find current rebate offers.",
              type: 'content'
            },
            {
              name: 'lblSubmit',
              title: "Enter offer and invoice date.",
              type: 'heading'
            },
            {
              name: 'offer',
              title: 'Offer',
              type: 'string',
              required: true
            },
            {
              name: 'invoiceDate',
              title: 'Invoice Date',
              type: 'date',
              required: true
            },
            {
              name: 'btnSubmit',
              title: "Continue >",
              type: 'button'
            }
          ]
        },
      ],
      onSuccess: 'productInfo',
      onError: 'home',
    },
    {
      pageId: 'productInfo',
      title: 'Product Information',
      serviceUrl: "https://u009kulxyc.execute-api.us-east-2.amazonaws.com/staging/productaddition",
      fields: [
        {
          name: 'lblProd',
          title: "Product Information",
          type: 'heading'
        },
        // {
        //   title: 'Invoice Id',
        //   name: 'invoiceId',
        //   type: 'string',
        //   required: true,
        // },
        // {
        //   name: 'productId',
        //   title: 'Product',
        //   type: 'string',
        //   minLength: 5,
        //   maxLength: 20,
        //   required: true,
        //   options: [
        //     { name: "First", value: "12J5" },
        //     { name: "Second", value: "45A77" },
        //     { name: "Third", value: "A48" }
        //   ]
        // },
        // {
        //   name: 'quantity',
        //   title: 'Quantity',
        //   type: 'number',
        //   required: true,
        // },

        // {
        //   name: 'btnSubmit',
        //   title: "Continue >",
        //   type: 'button'
        // }
      ],
      onSuccess: 'customerInfo',
      onError: 'productInfo'
    },
    {
      pageId: 'customerInfo',
      title: 'Customer Information',
      serviceUrl: "https://isff5c4lif.execute-api.us-east-2.amazonaws.com/staging/validateSubmission",
      fields: [
        {
          name: 'lblCust',
          title: "Customer Information",
          type: 'heading'
        },
        {
          name: 'customerFName',
          title: 'First Name',
          type: 'string',
          minLength: 3,
          maxLength: 20,
          required: true,
        },
        {
          name: 'customerLName',
          title: 'Last Name',
          type: 'string',
          minLength: 3,
          maxLength: 20,
          required: true,
        },
        {
          serviceUrl: 'address/validate',
          fields: [
            {
              name: 'customerAddress1',
              title: 'Address 1',
              type: 'string',
              minLength: 5,
              maxLength: 26,
              required: true,
            },
            {
              name: 'customerAddress2',
              title: 'Address 2',
              type: 'string',
              minLength: 5,
              maxLength: 26,
              required: false,
            },
            {
              serviceUrl:"./../../assets/states.json",
              name: 'customerState',
              title: 'State',
              type: 'string',
              minLength: 5,
              maxLength: 26,
              options: [ ],
              required: true,
            },
            {
              serviceUrl:"./../../assets/cities.json",
              name: 'customerCity',
              title: 'City',
              type: 'string',
              minLength: 5,
              maxLength: 20,
              required: true,
              options: [ ]
            },
            {
              name: 'zip',
              title: 'Zip',
              type: 'string',
              minLength: 5,
              maxLength: 10,
              required: true,
            },
            //Below fields added for current customer form testing
            {
              serviceUrl:"https://fwhtefxxre.execute-api.us-east-2.amazonaws.com/CORS/getRetailerInfo",
              title: 'Retailer Name',
              name: 'retailerName',
              type: 'string',
              options: [],
              required: true,
            },
            {
              title: 'Email',
              name: 'email',
              type: 'email',
              required: true,
            },
            {
              title: 'Phone',
              name: 'phone',
              type: 'string',
              required: true,
            }
            , {
              name: 'btnSubmit',
              title: "Submit",
              type: 'button'
            }

          ]
        }
      ]
      ,
      onSuccess: 'invoiceInfo',
      onError: 'invoiceInfo'
    },
    {
      pageId: 'invoiceInfo',
      serviceUrl: "https://4nuhe4idgb.execute-api.us-east-2.amazonaws.com/staging/create",
      title: 'Invoice Information',
      fields: [
        {
          serviceUrl: 'invoice/upload',
          fields: [
            {
              name: 'lblUpload',
              title: "Upload Invoice",
              type: 'heading'
            },
            // {
            //   name: 'invoiceImg1',
            //   type: 'file',
            //   size: 3.5,
            //   title: 'Invoice Image 1',
            //   required: true,
            // },
            // {
            //   name: 'invoiceImg2',
            //   type: 'file',
            //   size: 3.5,
            //   title: 'Invoice Image 2',
            //   required: false,
            // }
            // ,
            {
              name: 'btnSubmit',
              title: "Submit",
              type: 'button'
            }

          ]
        }
      ]
    }

  ]
};
